FROM openjdk:8-jdk-alpine
EXPOSE 8080
ADD target/*.jar stock-app.jar
ENTRYPOINT ["java","-jar","stock-app.jar"]