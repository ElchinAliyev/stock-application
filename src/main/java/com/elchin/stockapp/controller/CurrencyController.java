package com.elchin.stockapp.controller;

import com.elchin.stockapp.dto.request.CreateCategoryDTO;
import com.elchin.stockapp.dto.request.CreateCurrencyDTO;
import com.elchin.stockapp.dto.request.UpdateCategoryDTO;
import com.elchin.stockapp.dto.request.UpdateCurrencyDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.service.CurrencyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@Validated
@RestController
@RequestMapping("/currencies")
@RequiredArgsConstructor
@Api(description = "Currency controller endpoints")
public class CurrencyController {

    private final CurrencyService currencyService;

    @GetMapping
    @ApiOperation(value = "Get all currencies list")
    public ResponseEntity<?> getCurrencyList() {
        ApiResponse apiResponse = currencyService.getCurrencyList();
        return ResponseEntity.ok(apiResponse);
    }

    @PostMapping
    @ApiOperation(value = "Create new currency")
    public ResponseEntity<?> createCurrency(@RequestBody @Valid CreateCurrencyDTO createCurrencyDTO) {
        ApiResponse apiResponse = currencyService.createCurrency(createCurrencyDTO);
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update currency")
    public ResponseEntity<?> updateCurrency(@RequestBody @Valid UpdateCurrencyDTO updateCurrencyDTO) {
        ApiResponse apiResponse = currencyService.updateCurrency(updateCurrencyDTO);
        return ResponseEntity.ok(apiResponse);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete currency")
    public ResponseEntity<?> deactivateCurrencyById(@PathVariable("id") @Min(value = 1, message = "The value must be positive") int id) {
        ApiResponse apiResponse = currencyService.deactivateCurrencyById(id);
        return ResponseEntity.ok(apiResponse);
    }

}
