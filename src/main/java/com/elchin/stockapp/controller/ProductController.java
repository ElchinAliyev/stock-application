package com.elchin.stockapp.controller;

import com.elchin.stockapp.dto.request.CreateProductDTO;
import com.elchin.stockapp.dto.request.PageRequestDTO;
import com.elchin.stockapp.dto.request.UpdateProductDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
@Api(description = "Product controller endpoints")
public class ProductController {

    private final ProductService productService;

    @GetMapping
    @ApiOperation(value = "Get all products with pagination")
    public ResponseEntity<?> getAllProducts(@RequestParam(value = "page", defaultValue = "1") int page,
                                            @RequestParam(value = "rows", defaultValue = "10") int rows,
                                            @RequestParam(value = "orderName", defaultValue = "createDate") String orderName,
                                            @RequestParam(value = "orderType", defaultValue = "desc") String orderType) {

        PageRequestDTO pageRequestDto = new PageRequestDTO();
        pageRequestDto.setPage(page - 1);
        pageRequestDto.setRows(rows);
        pageRequestDto.setOrderName(orderName);
        pageRequestDto.setOrderType(orderType);
        ApiResponse apiResponse = productService.getAllProducts(pageRequestDto);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation(value = "Create new product")
    public ResponseEntity<?> createProduct(@RequestBody @Valid CreateProductDTO createProductDTO) {
        ApiResponse apiResponse = productService.createProduct(createProductDTO);
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update product")
    public ResponseEntity<?> updateProduct(@RequestBody @Valid UpdateProductDTO updateProductDTO) {
        ApiResponse apiResponse = productService.updateProduct(updateProductDTO);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Product")
    public ResponseEntity<?> deleteProduct(@PathVariable @Min(value = 1, message = "The value must be positive") long id) {
        ApiResponse apiResponse = productService.deactivateProductById(id);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

}
