package com.elchin.stockapp.controller;

import com.elchin.stockapp.dto.request.CreateCategoryDTO;
import com.elchin.stockapp.dto.request.UpdateCategoryDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@Validated
@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
@Api(description = "Category controller endpoints")
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping
    @ApiOperation(value = "Get all categories list")
    public ResponseEntity<?> getCategoryList() {
        ApiResponse apiResponse = categoryService.getAllCategories();
        return ResponseEntity.ok(apiResponse);
    }

    @PostMapping
    @ApiOperation(value = "Create new category")
    public ResponseEntity<?> createCategory(@RequestBody @Valid CreateCategoryDTO createCategoryDTO) {
        ApiResponse apiResponse = categoryService.createCategory(createCategoryDTO);
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update category")
    public ResponseEntity<?> updateCategory(@RequestBody @Valid UpdateCategoryDTO updateCategoryDTO) {
        ApiResponse apiResponse = categoryService.updateCategory(updateCategoryDTO);
        return ResponseEntity.ok(apiResponse);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete category")
    public ResponseEntity<?> deactivateCategoryById(@PathVariable("id") @Min(value = 1, message = "The value must be positive") long id) {
        ApiResponse apiResponse = categoryService.deactivateCategoryById(id);
        return ResponseEntity.ok(apiResponse);
    }

}
