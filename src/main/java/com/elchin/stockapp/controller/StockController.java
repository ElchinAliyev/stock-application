package com.elchin.stockapp.controller;

import com.elchin.stockapp.dto.request.CreateStockDTO;
import com.elchin.stockapp.dto.request.UpdateStockDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.service.StockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@Validated
@RestController
@RequestMapping("/stocks")
@RequiredArgsConstructor
@Api(description = "Stock controller endpoints")
public class StockController {

    private final StockService stockService;

    @GetMapping
    @ApiOperation(value = "Get all stocks list")
    public ResponseEntity<?> getStockList() {
        ApiResponse apiResponse = stockService.getAllStocks();
        return ResponseEntity.ok(apiResponse);
    }

    @PostMapping
    @ApiOperation(value = "Create new stock")
    public ResponseEntity<?> createStock(@RequestBody @Valid CreateStockDTO createStockDTO) {
        ApiResponse apiResponse = stockService.createStock(createStockDTO);
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update stock")
    public ResponseEntity<?> updateStock(@RequestBody @Valid UpdateStockDTO updateStockDTO) {
        ApiResponse apiResponse = stockService.updateStock(updateStockDTO);
        return ResponseEntity.ok(apiResponse);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete stock")
    public ResponseEntity<?> deactivateStockById(@PathVariable("id") @Min(value = 1, message = "The value must be positive") int id) {
        ApiResponse apiResponse = stockService.deactivateStockById(id);
        return ResponseEntity.ok(apiResponse);
    }
}
