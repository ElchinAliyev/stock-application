package com.elchin.stockapp.dto.response;

import com.elchin.stockapp.dto.request.PageRequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;

@Getter
@Setter
public class PageResponseDTO {
    private PageRequestDTO pageRequest;
    private long total;
    private long dataCount;
    private Object data;
}
