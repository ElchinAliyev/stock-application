package com.elchin.stockapp.dto.response;

import java.util.List;

public class ErrorResponse {
    private int httpCode;
    private List<String> message;

    public ErrorResponse() {
    }

    public ErrorResponse(int httpCode) {
        this.httpCode = httpCode;
    }

    public ErrorResponse(List<String> message) {
        this.message = message;
    }

    public ErrorResponse(int httpCode, List<String> message) {
        this.httpCode = httpCode;
        this.message = message;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }
}
