
package com.elchin.stockapp.dto.response;

import lombok.Data;
import java.io.Serializable;


@Data
public class CurrencyResponseDTO implements Serializable {

    private static final long serialVersionUID = 1463481782821666376L;

    private long id;
    private String name;
}
