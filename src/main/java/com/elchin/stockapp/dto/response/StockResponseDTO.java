
package com.elchin.stockapp.dto.response;

import lombok.Data;
import java.io.Serializable;


@Data
public class StockResponseDTO implements Serializable {

    private long id;
    private String name;
}
