package com.elchin.stockapp.dto.request;

import lombok.Data;

@Data
public class PageRequestDTO {

    private int page;
    private int rows;
    private String orderName;
    private String orderType;
}
