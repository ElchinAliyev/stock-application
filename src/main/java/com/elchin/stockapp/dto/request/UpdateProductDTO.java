package com.elchin.stockapp.dto.request;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Data
public class UpdateProductDTO {
    @Min(value = 1, message = "Id must be minimum 1")
    private long id;
    private String name;
    private String serialNumber;
    private String description;
    private BigDecimal buyingPrice;
    private BigDecimal sellingPrice;
    private long quantity;
    private long categoryId;
    private long currencyId;
    private long stockId;

}
