package com.elchin.stockapp.dto.request;

import lombok.Data;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CreateProductDTO implements Serializable {

    private long id;

    @NotEmpty(message = "Name can not be empty")
    private String name;

    @NotEmpty(message = "Serial number can not be empty")
    private String serialNumber;

    private String description;

    @DecimalMin(value = "0.000001", message = "The value must be valid")
    private BigDecimal buyingPrice;

    @DecimalMin(value = "0.000001", message = "The value must be valid")
    private BigDecimal sellingPrice;

    @Min(value = 1, message = "The value must be positive")
    private long quantity;

    @Min(value = 1, message = "The value must be positive")
    private long categoryId;

    @Min(value = 1, message = "The value must be positive")
    private long currencyId;

    @Min(value = 1, message = "The value must be positive")
    private long stockId;


}
