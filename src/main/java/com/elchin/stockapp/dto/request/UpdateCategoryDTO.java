package com.elchin.stockapp.dto.request;

import lombok.Data;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
public class UpdateCategoryDTO implements Serializable {
    @Min(value = 1, message = "Id must be minimum 1")
    private long id;
    @NotEmpty(message = "Name can not be empty")
    private String name;
}
