package com.elchin.stockapp.dto.request;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
public class CreateCurrencyDTO implements Serializable {

    private long id;
    @NotEmpty(message = "Name can not be empty")
    private String name;
}
