package com.elchin.stockapp.repository;

import com.elchin.stockapp.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long> {

    Optional<Currency> getCurrencyByIdAndIsEnabled(long id, int isEnabled);

    Optional<Currency> getCurrencyByNameAndIsEnabled(String name, int isEnabled);

    List<Currency> getCurrenciesByIsEnabled(int id);

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update Currency c set c.isEnabled = :isEnabled where c.id = :id")
    void deactivateCurrencyById(@Param("id") long id, @Param("isEnabled") int isEnabled);

}
