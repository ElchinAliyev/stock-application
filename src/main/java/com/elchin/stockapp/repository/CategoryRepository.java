package com.elchin.stockapp.repository;

import com.elchin.stockapp.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    Optional<Category> getCategoryByIdAndIsEnabled(long id, int isEnabled);

    Optional<Category> getCategoryByNameAndIsEnabled(String name, int isEnabled);

    List<Category> getAllByIsEnabled(int isEnabled);

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update Category c set c.isEnabled = :isEnabled where c.id = :id")
    void deactivateCategoryById(@Param("id") long id, @Param("isEnabled") int isEnabled);

}
