package com.elchin.stockapp.repository;

import com.elchin.stockapp.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface StockRepository extends JpaRepository<Stock, Long> {

    List<Stock> getAllByIsEnabled(int isEnabled);

    Optional<Stock> findStockByIdAndIsEnabled(long id, int isEnabled);

    Optional<Stock> getStockByNameAndIsEnabled(String name, int isEnabled);

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update Stock s set s.isEnabled = :isEnabled where s.id = :id")
    void deactivateStockById(@Param("id") long id, @Param("isEnabled") int isEnabled);
}
