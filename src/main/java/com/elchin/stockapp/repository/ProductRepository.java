package com.elchin.stockapp.repository;

import com.elchin.stockapp.model.Product;
import com.elchin.stockapp.repository.projection.ProductListResponseProjection;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Optional<Product> getProductByIdAndIsEnabled(long id, int isEnabled);

    Optional<Product> getProductBySerialNumberAndIsEnabled(String serialNumber, int isEnabled);

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update Product p set p.isEnabled=:isEnabled where p.id=:id")
    int deactivateProduct(@Param("id") long id, @Param("isEnabled") int isEnabled);


    @Query("select p.id as id,p.name as name,p.serialNumber as serialNumber , p.description as description, " +
            "p.buyingPrice as buyingPrice, p.sellingPrice as sellingPrice, p.quantity as quantity, " +
            "c.name as category, cu.name as currency, s.name as stock, p.createDate as createDate " +
            "from Product  p " +
            "inner  join p.category c " +
            "inner  join  p.currency cu " +
            "inner  join  p.stock s WHERE p.isEnabled = 1")
    List<ProductListResponseProjection> getAllProductsWithPagination(Pageable pageable);

    @Query("select count (p.id) from Product p where p.isEnabled = 1")
    Integer getTotalCount();
}
