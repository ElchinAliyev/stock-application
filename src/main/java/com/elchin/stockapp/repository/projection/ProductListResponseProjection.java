package com.elchin.stockapp.repository.projection;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface ProductListResponseProjection {
    Long getId();

    String getName();

    String getSerialNumber();

    String getDescription();

    BigDecimal getBuyingPrice();

    BigDecimal getSellingPrice();

    Long getQuantity();

    String getCategory();

    String getCurrency();

    String getStock();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime getCreateDate();
}
