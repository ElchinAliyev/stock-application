package com.elchin.stockapp.service;

import com.elchin.stockapp.dto.request.CreateCategoryDTO;
import com.elchin.stockapp.dto.request.UpdateCategoryDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.model.Category;

public interface CategoryService {

    ApiResponse getAllCategories();

    ApiResponse createCategory(CreateCategoryDTO createCategoryDTO);

    ApiResponse updateCategory(UpdateCategoryDTO updateCategoryDTO);

    Category getCategoryById(long id);

    ApiResponse deactivateCategoryById(long id);

}
