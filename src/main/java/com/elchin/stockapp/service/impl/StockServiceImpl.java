package com.elchin.stockapp.service.impl;

import com.elchin.stockapp.dto.request.CreateStockDTO;
import com.elchin.stockapp.dto.request.UpdateStockDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.dto.response.StockResponseDTO;
import com.elchin.stockapp.exception.model.EntityNotFoundException;
import com.elchin.stockapp.exception.model.SuchEntityIsExistException;
import com.elchin.stockapp.model.Stock;
import com.elchin.stockapp.repository.StockRepository;
import com.elchin.stockapp.service.StockService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class StockServiceImpl implements StockService {

    private final StockRepository stockRepository;
    private final ModelMapper modelMapper;

    @Override
    public ApiResponse getAllStocks() {
        List<Stock> stockList = stockRepository.getAllByIsEnabled(1);
        return new ApiResponse(true, convertEntitiesToDTOs(stockList));
    }

    @Override
    public ApiResponse createStock(CreateStockDTO createStockDTO) {
        Optional<Stock> stockOptional = stockRepository.getStockByNameAndIsEnabled(createStockDTO.getName(), 1);
        if (stockOptional.isPresent()) {
            throw new SuchEntityIsExistException("Stock is exists, with this name: " + createStockDTO.getName());
        }
        Stock stock = modelMapper.map(createStockDTO, Stock.class);
        stock.setId(0);
        stockRepository.save(stock);
        createStockDTO.setId(stock.getId());
        return new ApiResponse(true, createStockDTO);
    }

    @Override
    public ApiResponse updateStock(UpdateStockDTO updateStockDTO) {
        Stock stock = getStockById(updateStockDTO.getId());
        stock.setName(updateStockDTO.getName());
        stockRepository.save(stock);
        return new ApiResponse(true, updateStockDTO);
    }

    @Override
    public Stock getStockById(long id) {
        Optional<Stock> stockOptional = stockRepository.findStockByIdAndIsEnabled(id, 1);
        if (!stockOptional.isPresent()) {
            throw new EntityNotFoundException("Stock not found, with this id: " + id);
        }
        return stockOptional.get();
    }

    @Override
    public ApiResponse deactivateStockById(long id) {
        getStockById(id);
        stockRepository.deactivateStockById(id, 0);
        return new ApiResponse(true);
    }

    private List<StockResponseDTO> convertEntitiesToDTOs(List<Stock> stockList) {
        return stockList.stream().map(stock -> modelMapper.map(stock, StockResponseDTO.class)).collect(Collectors.toList());
    }
}
