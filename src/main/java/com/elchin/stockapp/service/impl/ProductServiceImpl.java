package com.elchin.stockapp.service.impl;

import com.elchin.stockapp.dto.request.CreateProductDTO;
import com.elchin.stockapp.dto.request.PageRequestDTO;
import com.elchin.stockapp.dto.request.UpdateProductDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.dto.response.PageResponseDTO;
import com.elchin.stockapp.exception.model.EntityNotFoundException;
import com.elchin.stockapp.exception.model.SuchEntityIsExistException;
import com.elchin.stockapp.model.Category;
import com.elchin.stockapp.model.Currency;
import com.elchin.stockapp.model.Product;
import com.elchin.stockapp.model.Stock;
import com.elchin.stockapp.repository.ProductRepository;
import com.elchin.stockapp.repository.projection.ProductListResponseProjection;
import com.elchin.stockapp.service.CategoryService;
import com.elchin.stockapp.service.CurrencyService;
import com.elchin.stockapp.service.ProductService;
import com.elchin.stockapp.service.StockService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryService categoryService;
    private final CurrencyService currencyService;
    private final StockService stockService;
    private final ModelMapper modelMapper;

    @Override
    public ApiResponse getAllProducts(PageRequestDTO pageRequestDto) {
        PageRequest pageRequest = PageRequest.of(pageRequestDto.getPage(), pageRequestDto.getRows(), Sort.by(Sort.Direction.valueOf(pageRequestDto.getOrderType().toUpperCase()), pageRequestDto.getOrderName()));

        List<ProductListResponseProjection> projections = productRepository.getAllProductsWithPagination(pageRequest);
        Integer totalCount = productRepository.getTotalCount();
        PageResponseDTO pageResponseDTO = new PageResponseDTO();
        pageRequestDto.setPage(pageRequestDto.getPage() + 1);
        pageResponseDTO.setPageRequest(pageRequestDto);
        pageResponseDTO.setTotal(totalCount != null ? totalCount : 0);
        pageResponseDTO.setData(projections);
        pageResponseDTO.setDataCount(projections.size());

        return new ApiResponse(true, pageResponseDTO);
    }


    @Override
    public ApiResponse createProduct(CreateProductDTO createProductDTO) {
        Optional<Product> optionalProduct = productRepository.getProductBySerialNumberAndIsEnabled(createProductDTO.getSerialNumber(), 1);
        if (optionalProduct.isPresent()) {
            throw new SuchEntityIsExistException("Such product is exists, with this serial number: " + createProductDTO.getSerialNumber());
        }

        Category category = categoryService.getCategoryById(createProductDTO.getCategoryId());
        Currency currency = currencyService.getCurrencyById(createProductDTO.getCurrencyId());
        Stock stock = stockService.getStockById(createProductDTO.getStockId());

        Product product = modelMapper.map(createProductDTO, Product.class);
        product.setCategory(category);
        product.setCurrency(currency);
        product.setStock(stock);
        product.setId(0);
        product = productRepository.save(product);
        createProductDTO.setId(product.getId());
        return new ApiResponse(true, createProductDTO);
    }

    @Override
    public ApiResponse updateProduct(UpdateProductDTO updateProductDTO) {
        Product product = getProductById(updateProductDTO.getId());

        if (updateProductDTO.getName() != null && !updateProductDTO.getName().isEmpty()) {
            product.setName(updateProductDTO.getName());
        }
        if (updateProductDTO.getSerialNumber() != null && !updateProductDTO.getSerialNumber().isEmpty()) {
            product.setSerialNumber(updateProductDTO.getSerialNumber());
        }
        if (updateProductDTO.getDescription() != null && !updateProductDTO.getDescription().isEmpty()) {
            product.setDescription(updateProductDTO.getDescription());
        }
        if (updateProductDTO.getBuyingPrice() != null && updateProductDTO.getBuyingPrice().compareTo(BigDecimal.ZERO) > 0) {
            product.setBuyingPrice(updateProductDTO.getBuyingPrice());
        }
        if (updateProductDTO.getSellingPrice() != null && updateProductDTO.getSellingPrice().compareTo(BigDecimal.ZERO) > 0) {
            product.setSellingPrice(updateProductDTO.getSellingPrice());
        }
        if (updateProductDTO.getQuantity() > 0) {
            product.setQuantity(updateProductDTO.getQuantity());
        }
        if (updateProductDTO.getCategoryId() > 0) {
            product.setCategory(categoryService.getCategoryById(updateProductDTO.getCategoryId()));
        }
        if (updateProductDTO.getCurrencyId() > 0) {
            product.setCurrency(currencyService.getCurrencyById(updateProductDTO.getCurrencyId()));
        }
        if (updateProductDTO.getStockId() > 0) {
            product.setStock(stockService.getStockById(updateProductDTO.getStockId()));
        }
        productRepository.save(product);

        return new ApiResponse(true, updateProductDTO);
    }

    @Override
    public Product getProductById(long id) {
        Optional<Product> productOptional = productRepository.getProductByIdAndIsEnabled(id, 1);
        if (!productOptional.isPresent()) {
            throw new EntityNotFoundException("Product not found, with this id: " + id);
        }
        return productOptional.get();
    }


    @Override
    public ApiResponse deactivateProductById(long id) {
        getProductById(id);
        productRepository.deactivateProduct(id, 0);
        return new ApiResponse(true);
    }


}
