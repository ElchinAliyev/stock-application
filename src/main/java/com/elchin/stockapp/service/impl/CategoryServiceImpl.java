package com.elchin.stockapp.service.impl;


import com.elchin.stockapp.dto.request.CreateCategoryDTO;
import com.elchin.stockapp.dto.request.UpdateCategoryDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.dto.response.CategoryResponseDTO;
import com.elchin.stockapp.exception.model.EntityNotFoundException;
import com.elchin.stockapp.exception.model.SuchEntityIsExistException;
import com.elchin.stockapp.model.Category;
import com.elchin.stockapp.repository.CategoryRepository;
import com.elchin.stockapp.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Transactional
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final ModelMapper modelMapper;

    @Override
    public ApiResponse getAllCategories() {
        List<Category> categoryList = categoryRepository.getAllByIsEnabled(1);
        return new ApiResponse(true, convertEntitiesToDTOs(categoryList));
    }

    @Override
    public ApiResponse createCategory(CreateCategoryDTO createCategoryDTO) {
        Optional<Category> categoryOptional = categoryRepository.getCategoryByNameAndIsEnabled(createCategoryDTO.getName(), 1);
        if (categoryOptional.isPresent()) {
            throw new SuchEntityIsExistException("Category is exists, with this name: " + createCategoryDTO.getName());
        }
        Category category = modelMapper.map(createCategoryDTO, Category.class);
        category.setId(0);
        categoryRepository.save(category);
        createCategoryDTO.setId(category.getId());
        return new ApiResponse(true, createCategoryDTO);
    }

    @Override
    public ApiResponse updateCategory(UpdateCategoryDTO updateCategoryDTO) {
        Category category = getCategoryById(updateCategoryDTO.getId());
        category.setName(updateCategoryDTO.getName());
        categoryRepository.save(category);
        return new ApiResponse(true, updateCategoryDTO);
    }

    @Override
    public Category getCategoryById(long id) {
        Optional<Category> optionalCategory = categoryRepository.getCategoryByIdAndIsEnabled(id, 1);
        if (!optionalCategory.isPresent()) {
            throw new EntityNotFoundException("Category not found, with this id: " + id);
        }
        return optionalCategory.get();
    }

    @Override
    public ApiResponse deactivateCategoryById(long id) {
        getCategoryById(id);
        categoryRepository.deactivateCategoryById(id, 0);
        return new ApiResponse(true);
    }

    private List<CategoryResponseDTO> convertEntitiesToDTOs(List<Category> categoryList) {
        return categoryList.stream().map(category -> modelMapper.map(category, CategoryResponseDTO.class)).collect(Collectors.toList());
    }

}
