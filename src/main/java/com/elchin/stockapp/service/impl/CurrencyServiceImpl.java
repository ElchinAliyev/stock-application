package com.elchin.stockapp.service.impl;

import com.elchin.stockapp.dto.request.CreateCurrencyDTO;
import com.elchin.stockapp.dto.request.UpdateCurrencyDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.dto.response.CurrencyResponseDTO;
import com.elchin.stockapp.exception.model.EntityNotFoundException;
import com.elchin.stockapp.exception.model.SuchEntityIsExistException;
import com.elchin.stockapp.model.Currency;
import com.elchin.stockapp.repository.CurrencyRepository;
import com.elchin.stockapp.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Transactional
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;
    private final ModelMapper modelMapper;


    @Override
    public ApiResponse getCurrencyList() {
        List<Currency> currencyList = currencyRepository.getCurrenciesByIsEnabled(1);
        return new ApiResponse(true, convertEntitiesToDTOs(currencyList));
    }

    @Override
    public ApiResponse createCurrency(CreateCurrencyDTO currencyDTO) {
        Optional<Currency> currencyOptional = currencyRepository.getCurrencyByNameAndIsEnabled(currencyDTO.getName(), 1);
        if (currencyOptional.isPresent()) {
            throw new SuchEntityIsExistException("Currency is exists, with this name: " + currencyDTO.getName());
        }

        Currency currency = modelMapper.map(currencyDTO, Currency.class);
        currency.setId(0);
        currencyRepository.save(currency);
        currencyDTO.setId(currency.getId());

        return new ApiResponse(true, currencyDTO);
    }

    @Override
    public ApiResponse updateCurrency(UpdateCurrencyDTO currencyDTO) {
        Currency currency = getCurrencyById(currencyDTO.getId());
        currency.setName(currencyDTO.getName());
        currencyRepository.save(currency);
        return new ApiResponse(true, currencyDTO);
    }

    @Override
    public Currency getCurrencyById(long id) {
        Optional<Currency> currencyOptional = currencyRepository.getCurrencyByIdAndIsEnabled(id, 1);
        if (!currencyOptional.isPresent()) {
            throw new EntityNotFoundException("Currency not found, with this id: " + id);
        }
        return currencyOptional.get();
    }

    @Override
    public ApiResponse deactivateCurrencyById(long id) {
        getCurrencyById(id);
        currencyRepository.deactivateCurrencyById(id, 0);
        return new ApiResponse(true);
    }

    private List<CurrencyResponseDTO> convertEntitiesToDTOs(List<Currency> currencyList) {
        return currencyList.stream().map(currency -> modelMapper.map(currency, CurrencyResponseDTO.class)).collect(Collectors.toList());
    }
}
