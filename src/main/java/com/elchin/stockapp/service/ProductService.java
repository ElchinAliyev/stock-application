package com.elchin.stockapp.service;

import com.elchin.stockapp.dto.request.CreateProductDTO;
import com.elchin.stockapp.dto.request.PageRequestDTO;
import com.elchin.stockapp.dto.request.UpdateProductDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.model.Product;

public interface ProductService {
    ApiResponse getAllProducts(PageRequestDTO pageRequestDTO);

    ApiResponse createProduct(CreateProductDTO createProductDTO);

    ApiResponse updateProduct(UpdateProductDTO updateProductDTO);

    Product getProductById(long id);

    ApiResponse deactivateProductById(long id);

}
