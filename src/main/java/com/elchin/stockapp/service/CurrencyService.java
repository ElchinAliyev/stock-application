package com.elchin.stockapp.service;


import com.elchin.stockapp.dto.request.CreateCurrencyDTO;
import com.elchin.stockapp.dto.request.UpdateCurrencyDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.model.Currency;

public interface CurrencyService {

    ApiResponse getCurrencyList();

    ApiResponse createCurrency(CreateCurrencyDTO currencyDTO);

    ApiResponse updateCurrency(UpdateCurrencyDTO currencyDTO);

    Currency getCurrencyById(long id);

    ApiResponse deactivateCurrencyById(long id);

}
