package com.elchin.stockapp.service;

import com.elchin.stockapp.dto.request.CreateStockDTO;
import com.elchin.stockapp.dto.request.UpdateStockDTO;
import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.model.Stock;

public interface StockService {

    ApiResponse getAllStocks();

    ApiResponse createStock(CreateStockDTO createStockDTO);

    ApiResponse updateStock(UpdateStockDTO updateStockDTO);

    Stock getStockById(long id);

    ApiResponse deactivateStockById(long id);

}
