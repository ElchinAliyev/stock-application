package com.elchin.stockapp.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;


@Entity
@Table(name = "PRODUCTS")
@Data
public class Product extends AbstractEntity implements Serializable {


    @Column(name = "NAME")
    private String name;

    @Column(name = "SERIAL_NUMBER")
    private String serialNumber;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "BUYING_PRICE")
    private BigDecimal buyingPrice;

    @Column(name = "SELLING_PRICE")
    private BigDecimal sellingPrice;

    @Column(name = "QUANTITY")
    private long quantity;

    @JoinColumn(name = "CATEGORY_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    @JoinColumn(name = "CURRENCY_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Currency currency;

    @JoinColumn(name = "STOCK_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Stock stock;

    @Column(name = "IS_ENABLED")
    private int isEnabled = 1;


}
