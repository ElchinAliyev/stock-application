package com.elchin.stockapp.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "CURRENCIES")
@Data
public class Currency extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -6343971059763777337L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "IS_ENABLED")
    private int isEnabled = 1;

    @OneToMany(mappedBy = "currency", fetch = FetchType.LAZY)
    private List<Product> productList;

}
