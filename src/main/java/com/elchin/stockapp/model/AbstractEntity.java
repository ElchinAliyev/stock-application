package com.elchin.stockapp.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
@Data
public abstract class AbstractEntity implements Serializable {
    private static final long serialVersionUID = 270383254869437136L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "created", updatable = false)
    private LocalDateTime createDate;

    @Column(name = "updated")
    private LocalDateTime updateDate;


    @PrePersist
    public void toCreate() {
        setCreateDate(LocalDateTime.now());
    }

    @PreUpdate
    public void toUpdate() {
        updateDate = LocalDateTime.now();
    }

}
