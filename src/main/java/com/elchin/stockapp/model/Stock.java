
package com.elchin.stockapp.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "STOCKS")
@Data
public class Stock extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 1838764432740746860L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "IS_ENABLED")
    private int isEnabled = 1;

    @OneToMany(mappedBy = "stock", fetch = FetchType.LAZY)
    private List<Product> productList;


    
}
