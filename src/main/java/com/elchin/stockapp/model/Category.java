package com.elchin.stockapp.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "CATEGORIES")
@Data
public class Category extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    @Column(name = "NAME")
    private String name;

    @Column(name = "IS_ENABLED")
    private int isEnabled = 1;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    private List<Product> productList;

}
