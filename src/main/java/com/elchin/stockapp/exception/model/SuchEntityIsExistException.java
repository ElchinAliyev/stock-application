package com.elchin.stockapp.exception.model;

public class SuchEntityIsExistException extends RuntimeException {
    public SuchEntityIsExistException() {
        super();
    }

    public SuchEntityIsExistException(String message) {
        super(message);
    }
}
