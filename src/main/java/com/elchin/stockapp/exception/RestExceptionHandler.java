package com.elchin.stockapp.exception;

import com.elchin.stockapp.dto.response.ApiResponse;
import com.elchin.stockapp.dto.response.ErrorResponse;
import com.elchin.stockapp.exception.model.EntityNotFoundException;
import com.elchin.stockapp.exception.model.SuchEntityIsExistException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.debug("MethodArgumentNotValid: ", ex);
        List<String> messageList = new ArrayList<>();
        BindingResult bindingResult = ex.getBindingResult();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        fieldErrors.forEach(fieldError -> {
            messageList.add(fieldError.getField() + " is  not valid: " + fieldError.getDefaultMessage());
        });

        return new ResponseEntity<>(new ApiResponse(false, new ErrorResponse(400, messageList)), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> handleConstraintViolationException(ConstraintViolationException ex) {
        logger.debug("ConstraintViolationException: ", ex);
        ex.printStackTrace();
        List<String> messageList = new ArrayList<>();
        ex.getConstraintViolations().forEach(constraintViolation -> {
            messageList.add(constraintViolation.getMessage());
        });

        return new ResponseEntity<>(new ApiResponse(false, new ErrorResponse(400, messageList)), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<?> handleEntityNotFoundException(EntityNotFoundException ex) {
        logger.debug("EntityNotFoundException: ", ex);
        String message = ex.getLocalizedMessage() != null ? ex.getLocalizedMessage() : "Entity not found.";
        return new ResponseEntity<>(new ApiResponse(false, new ErrorResponse(400, Collections.singletonList(message))), HttpStatus.valueOf(HttpStatus.BAD_REQUEST.value()));
    }

    @ExceptionHandler(SuchEntityIsExistException.class)
    public ResponseEntity<?> handleSuchEntityIsExistException(SuchEntityIsExistException ex) {
        logger.debug("SuchEntityIsExistException: ", ex);
        String message = ex.getLocalizedMessage() != null ? ex.getLocalizedMessage() : "Such entity is exists.";
        return new ResponseEntity<>(new ApiResponse(false, new ErrorResponse(400, Collections.singletonList(message))), HttpStatus.valueOf(HttpStatus.BAD_REQUEST.value()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleRestOfExceptions(Exception ex) {
        logger.error("Uncaught error:", ex);
        return new ResponseEntity<>(new ApiResponse(false, new ErrorResponse(500, Collections.singletonList("Uncaught error"))), HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }

}
